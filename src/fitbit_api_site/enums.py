from enum import Enum


class Permission(Enum):
    ADMIN = 'Admin'
