import datetime

import pytz
from apscheduler.schedulers.background import BackgroundScheduler

# noinspection PyUnresolvedReferences
# running tasks requires context
from fitbit_api_site import app
from fitbit_api_site.views import refresh_activities

scheduler = BackgroundScheduler(timezone=pytz.timezone('America/Los_Angeles'))


@scheduler.scheduled_job('interval', days=1,
                         start_date=datetime.datetime(year=2016, month=9, day=30),
                         misfire_grace_time=30)
def update_from_fitbit():
    print(refresh_activities())


scheduler.start()
