import sys
from enum import Enum

from flask_alembic import Alembic
from flask_alembic.cli.script import manager as alembic_manager
from flask_migrate import Migrate
from flask_script import Manager

from fitbit_api_site import db, app, prepare_config


class Option(Enum):
    CREATE = 1
    MANAGE = 2


def create():
    app.config['ALEMBIC'] = {
        'script_location': app.config['SQLALCHEMY_MIGRATE_REPO'],
        'sqlalchemy.url': app.config['SQLALCHEMY_DATABASE_URI']
    }
    db.create_all()
    alembic = Alembic()
    alembic.init_app(app)


def manage():
    del sys.argv[1]  # remove configuration parameter
    app.config['ALEMBIC'] = {
        'script_location': app.config['SQLALCHEMY_MIGRATE_REPO'],
        'sqlalchemy.url': app.config['SQLALCHEMY_DATABASE_URI']
    }

    migrate = Migrate(app, db)
    manager = Manager(app)
    alembic = Alembic(app)

    manager.add_command('db', alembic_manager)
    manager.run()


def main_create():
    main(route=Option.CREATE)


def main(route=Option.MANAGE):
    if len(sys.argv) <= 1:
        if Option.MANAGE:
            usage = 'Usage: db_manage.py PATH_TO_PYTHON_CONFIG_FILE db revision "Revision Message" THEN db upgrade'
        else:
            usage = 'Usage: db_create.py PATH_TO_PYTHON_CONFIG_FILE'
        print(usage)
        raise ValueError(usage)

    try:
        app.config.from_pyfile(sys.argv[1])
    except:
        print('Not a file.')
        raise

    prepare_config()

    if route == Option.MANAGE:
        manage()
    elif route == Option.CREATE:
        create()
    else:
        raise ValueError('Unrecognized option: {}'.format(route))
