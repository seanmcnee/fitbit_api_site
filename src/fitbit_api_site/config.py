# fitbit app oauth2 settings
CLIENT_ID = 'XXXXXX'  # get from fitbit
CLIENT_SECRET = 'XXXXXX'  # get from fitbit
CALLBACK = 'https://DOMAIN/APP/callback'  # define & add this URL to your fitbit app settings on dev.fitbit.com

# database settings
SQLALCHEMY_DATABASE_URI = "mysql://USERNAME:PASSWORD@DBSERVER:3306/DATABASE"  # match to your running mysql server
SQLALCHEMY_TRACK_MODIFICATIONS = "True"

# application security settings
SECRET_KEY = '1353084696007018235875672678084656087916123306427631453248'  # must be at least 22 chars long
SECURITY_PASSWORD_HASH = "bcrypt"  # don't change this
SECURITY_PASSWORD_SALT = "1234567890123456789012"  # must be 22 characters long

# application settings
BASE_DIR = r'/FULL/PATH/TO/CODE/fitbit_api_site'
START_DATE = '2018-04-26'
END_DATE = '2019-04-30'
