import base64
import json
from datetime import datetime, timedelta

import dateutil.parser as dtparser
from fitbit import Fitbit
from fitbit import FitbitOauth2Client


class MyFitbit(Fitbit):

    def __init__(self, client_id, client_secret, **kwargs):
        super().__init__(client_id, client_secret, **kwargs)
        self.client = FitbitOauth2Client(client_id, client_secret, **kwargs)

    def get_access_token(self):
        return self.client.session.token['access_token']

    def get_refresh_token(self):
        return self.client.session.token['refresh_token']

    def _format_date(self, dt):
        return '{}-{:02d}-{:02d}'.format(dt.year, dt.month, dt.day)

    def _get_date_or_today(self, date, stringify=True):
        if date:
            dt = dtparser.parse(date)
        else:
            dt = datetime.today()
        if stringify:
            return self._format_date(dt)
        else:
            return dt

    def _get_recent_date(self, date, stringify=True):
        if date:
            dt = dtparser.parse(date)
        else:
            dt = datetime.today()
        dt = min(dt, datetime.today())
        if stringify:
            return self._format_date(dt)
        else:
            return dt

    def get_activity(self, user_id, tracker=False, resource='steps', base_date=None, end_date=None,
                     period='1d'):

        base_date = self._get_date_or_today(base_date)
        if end_date:
            end_date = self._get_recent_date(end_date)

        url = '{0}/{1}/user/{2}/activities/{resource}/date/{date}/{end}.json'.format(
            *self._get_common_args(user_id),
            resource='tracker/{}'.format(resource) if tracker else resource,
            date=base_date if base_date else datetime.today(),
            end=end_date if end_date else period
        )
        return self.make_request(url)

    def get_sleep_range(self, user_id, base_date=None, end_date=None, period='1d'):
        base_date = self._get_date_or_today(base_date, stringify=False)
        if end_date or period == 'max':
            if end_date:
                end_date = self._get_recent_date(end_date, stringify=False)
            else:
                end_date = datetime.today()
            days = (end_date - base_date).days + 1
        else:
            if period[-1] == 'd':
                days = int(period[:-1])
            elif period[-1] == 'w':
                days = int(period[:-1]) * 7
            elif period[-1] == 'm':
                days = int(period[:-1]) * 30
            elif period[-1] == 'y':
                days = int(period[:-1]) * 365
            else:
                days = 0

        data = []
        for i in range(days):
            curr_date = base_date + timedelta(i)
            res = self.get_sleep_for_user(curr_date, user_id)
            res['dateTime'] = self._format_date(curr_date)
            data.append(res)
        return {'sleep': data}

    def get_sleep_for_user(self, date, user_id='-'):
        url = "{0}/{1}/user/{user_id}/sleep/date/{year}-{month}-{day}.json".format(
            *self._get_common_args(),
            user_id=user_id,
            year=date.year,
            month=date.month,
            day=date.day
        )
        return self.make_request(url)

    def get_tracker_calories(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'calories', base_date, end_date, period)

    def get_tracker_steps(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'steps', base_date, end_date, period)

    def get_tracker_distance(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'distance', base_date, end_date, period)

    def get_tracker_floors(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'floors', base_date, end_date, period)

    def get_tracker_elevation(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'elevation', base_date, end_date, period)

    def get_tracker_sedentary_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'minutesSedentary', base_date, end_date, period)

    def get_tracker_lightly_active_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'minutesLightlyActive', base_date, end_date, period)

    def get_tracker_fairly_active_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'minutesFairlyActive', base_date, end_date, period)

    def get_tracker_very_active_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'minutesVeryActive', base_date, end_date, period)

    def get_tracker_activity_calories(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, True, 'activityCalories', base_date, end_date, period)

    def get_calories(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'calories', base_date, end_date, period)

    def get_steps(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'steps', base_date, end_date, period)

    def get_distance(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'distance', base_date, end_date, period)

    def get_floors(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'floors', base_date, end_date, period)

    def get_elevation(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'elevation', base_date, end_date, period)

    def get_sedentary_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'minutesSedentary', base_date, end_date, period)

    def get_lightly_active_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'minutesLightlyActive', base_date, end_date, period)

    def get_fairly_active_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'minutesFairlyActive', base_date, end_date, period)

    def get_very_active_minutes(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'minutesVeryActive', base_date, end_date, period)

    def get_activity_calories(self, user_id, base_date=None, end_date=None, period='1d'):
        return self.get_activity(user_id, False, 'activityCalories', base_date, end_date, period)

