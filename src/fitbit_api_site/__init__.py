from flask import Flask
import os
import cherrypy
import argparse
from fitbit_api_site import mylogging
from paste.translogger import TransLogger
from flask_sqlalchemy import SQLAlchemy

BASE_DIR = os.path.join(os.path.dirname(__file__))
STATIC_DIR = os.path.join(BASE_DIR, 'static')
TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')

app = Flask(__name__, static_folder=STATIC_DIR, template_folder=TEMPLATE_DIR)
app.debug = True
app.config.from_object('fitbit_api_site.config')  # read config.py file
app.secret_key = app.config['SECRET_KEY']

db = SQLAlchemy(app)


# avoid circular imports
import fitbit_api_site.views
import fitbit_api_site.tasks


def _make_dir(d):
    os.makedirs(d, exist_ok=True)
    return d


def prepare_config():
    app.config['LOG_DIR'] = _make_dir(os.path.join(app.config['BASE_DIR'], 'logs'))
    app.config['SQLALCHEMY_MIGRATE_REPO'] = _make_dir(os.path.join(app.config['BASE_DIR'], 'migrations'))
    app.config['OUT_DIR'] = _make_dir(os.path.join(app.config['BASE_DIR'], 'out'))

    os.makedirs(app.config['LOG_DIR'], exist_ok=True)
    handler = mylogging.StaticTimedRotatingFileHandler(
        os.path.join(app.config['LOG_DIR'], 'log_file'), "midnight", 1)
    handler.suffix = '%Y-%m-%d'
    app.logger.addHandler(handler)


def run_server(port=8090):
    app_logged = TransLogger(app, logger=app.logger, setup_console_handler=False)
    conf = {
        '/': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': app.static_folder,
            'tools.staticdir.debug': True,
        },
    }
    cherrypy.tree.graft(app_logged, '/')
    cherrypy.tree.mount(None, '/static', config=conf)
    cherrypy.config.update(
        {
            'engine.autoreload.on': False,
            'log.screen': True,
            'server.socket_port': port,
            'server.socket_host': '0.0.0.0',
        }
    )
    if hasattr(cherrypy.engine, 'signal_handler'):
        cherrypy.engine.signal_handler.subscribe()
    cherrypy.engine.start()
    cherrypy.engine.block()


def main():
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument('--port', default=8090, type=int,
                        help='Specify port to run file on.')
    # parser.add_argument('--config', required=True,
    #                     help='Base directory to use for storing files.')
    parser.add_argument('--https-proxy', default=None,
                        help='Specify https proxy for application. This will handle fitbit callback.')
    args = parser.parse_args()

    # app.config.from_pyfile(args.config)
    if args.https_proxy:
        os.environ['HTTPS_PROXY'] = args.https_proxy
    prepare_config()
    run_server(port=args.port)


if __name__ == '__main__':
    main()


