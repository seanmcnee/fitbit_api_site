# fitbit_api_site #
An application that gathers and organizes Fitbit information for multiple Fitbit users through the Fitbit API. It is 
designed to be used by researchers to track the Fitbit usage patterns of subjects.

## Design ##
This web application manages the login and Fitbit usage data for a number of users. All data is stored in a local 
database. Data can be viewed in the app or downloaded for further analysis.

This application uses flask and runs on a cherrypy server, using the very nice python-fitbit library 
(https://github.com/orcasgit/python-fitbit) to handle Fitbit authentication.

## Dependencies ##
This application has the following sets of dependencies:

1. Python 3.5 (or later)

   All required python packages are listed in `requirements.txt`.  Use a Python Virtual Environment (`virtualenv`) \
   to manage the packages is highly recommended. The app is not compatible with Python 2.7.x. This application uses 
   flask and runs on a cherrypy server, using the python-fitbit library (https://github.com/orcasgit/python-fitbit) 
   to handle Fitbit authentication.
    
2. MySql or MariaDB

   Tested with Maria DB 10.x, but should work with MySql. You may need to install the Python and MariaDB/MySQL 
   development headers and libraries. The app assumes a dedicated database has been created and is available.
   The application itself can create the required tables.

## Getting Setup ##
0. Ensure MySQL or MariaDB availability, and that you have a database and a username/password
1. Register web application with Fitbit.com, create a web application which uses a "server" oauth2 path
2. Download/clone module from bitbucket
3. Create and enable python virtualenv
4. Run `python setup.py install`
5. Modify the configuration file `config.py`
6. Setup database tables by running `python3 db_create.py config.py`, note that the config file name is required
7. Add users via code changes to `views.py`
8. To run the app: `python3 runserver.py`
9. Direct browser to 127.0.0.1:8090

## Update Configuration ##
The `config.py` file needs to have some parameters configured:

1. `CLIENT_ID` 
    from your fitbit web app (something like `12ABCD`)
2. `CLIENT_SECRET`
    from your fitbit web app (something like `ab123c4de567890fab123c4de567f890`)
3. `CALLBACK`
    This is a public facing URL that `api.fitbit.com` can talk to in order to send data to your application.
    If your app is not on the internet, then it needs access to a reverse proxy server to make that connection work 
4. `SQLALCHEMY_DATABASE_URI`
    This is the ODBC formatted string to connect to the mysql database
6. `SECRET_KEY`
    This is used to secure cookies with your end clients to prevent connection spoofing. It should be long and 
    random. Only use letters and numbers
7. `BASE_DIR`
    The full path on your machine to this code's location
8. `SECURITY_PASSWORD_HASH`
    Algorithm used to secure login passwords. Don't change this.
9. `SECURITY_PASSWORD_SALT`  
    Used by `SECURITY_PASSWORD_HASH`, must be exactly 22 characters long and should be random. Only use letters 
    and numbers. Should be different from `SECRET_KEY`

## Adding Users ##
As a security precaution, there is no mechanism for creating new users to access this application via the web interface.
Users are added to the application via a call to `create_users_and_roles()` located in `views.py`. To add or change
users, modify this code. 

Users can be removed by deleting their row directly from the database.

## Adding Fitbit accounts ##
Fitbit accounts are added via the main UI screen. You will need to keep track of the fitbit users' usernames and 
passwords separately.

## Troubleshooting ##

### No connection could be made because... ###
If you are getting an error like:

`No connection could be made because the target machine actively refused it`

Try including the `--https-proxy` option when running `start-fitbit-site`.

### Handling SQLAlchemy Migrations ###
Should additional tables become required in the application:

1. Update `models.py` with new classes/tables
2. Prepare migration with `python3 db_manage config.py db revision "Name of changes"`
3. Review generated file in `BASE_DIR/migrations` to ensure appropriate actions will be taken.
4. To make changes, run `python3 db_manage config.py db upgrade`
